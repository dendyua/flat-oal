
#include "Oal.hpp"

#include <stdexcept>

#include "Debug.hpp"




namespace Flat {
namespace Oal {




const char * attrName(const ALCint key) noexcept
{
#define FLAT_OAL_ATTR(n) case n: return #n;

	switch (key) {
	FLAT_OAL_ATTR(ALC_MAJOR_VERSION)
	FLAT_OAL_ATTR(ALC_MINOR_VERSION)
	FLAT_OAL_ATTR(ALC_FREQUENCY)
	FLAT_OAL_ATTR(ALC_REFRESH)
	FLAT_OAL_ATTR(ALC_SYNC)
	FLAT_OAL_ATTR(ALC_MONO_SOURCES)
	FLAT_OAL_ATTR(ALC_STEREO_SOURCES)

	FLAT_OAL_ATTR(ALC_HRTF_SOFT)
	FLAT_OAL_ATTR(ALC_HRTF_STATUS_SOFT)
	FLAT_OAL_ATTR(ALC_NUM_HRTF_SPECIFIERS_SOFT)

	FLAT_OAL_ATTR(ALC_EFX_MAJOR_VERSION)
	FLAT_OAL_ATTR(ALC_EFX_MINOR_VERSION)
	FLAT_OAL_ATTR(ALC_MAX_AUXILIARY_SENDS)

	FLAT_OAL_ATTR(ALC_OUTPUT_LIMITER_SOFT)

	case 0x199B: return "ALC_MAX_AMBISONIC_ORDER_SOFT";
	}

#undef FLAT_OAL_ATTR

	return nullptr;
}




void checkAlcError()
{
	const ALCenum error = ::alcGetError(nullptr);
	FLAT_CHECK(error == ALC_NO_ERROR) << int(error);
}


void checkAlcError(const Device & device)
{
	const ALCenum error = ::alcGetError(device.handle);
	FLAT_CHECK(error == ALC_NO_ERROR) << int(error);
}


void checkError()
{
	const ALenum error = ::alGetError();
	FLAT_CHECK(error == AL_NO_ERROR) << int(error);
}


static void checkErrorQuietly() noexcept
{
	const ALenum error = ::alGetError();
	if (FLAT_UNLIKELY(error != AL_NO_ERROR)) {
		FLAT_ERROR << int(error);
	}
}




Device Device::open(const char * const name)
{
	::ALCdevice * const handle = ::alcOpenDevice(name);
	checkAlcError();
	if (!handle) {
		throw std::invalid_argument("no AL device");
	}
	return Device(handle);
}




Context Context::create(const Device & device, const ALCint * const attrs)
{
	ALCcontext * const handle = ::alcCreateContext(device.handle, attrs);
	checkAlcError(device);
	if (!handle) {
		throw std::runtime_error("no AL context");
	}
	return Context(handle);
}




Buffer Buffer::create()
{
	ALuint id;
	::alGenBuffers(1, &id);
	checkError();
	return Buffer(id);
}


Buffer::~Buffer() noexcept
{
	if (id != AL_NONE) {
		::alDeleteBuffers(1, &id);
		checkErrorQuietly();
	}
}




Source Source::create()
{
	ALuint id;
	::alGenSources(1, &id);
	checkError();
	return Source(id);
}


Source::~Source() noexcept
{
	if (id != AL_NONE) {
		::alDeleteSources(1, &id);
		checkErrorQuietly();
	}
}




}}
