
#pragma once

#include <algorithm>

#include <AL/alc.h>
#include <AL/alext.h>
#include <AL/efx.h>

#include <Flat/Debug/Exception.hpp>




namespace Flat {
namespace Oal {




struct Device;




extern FLAT_OAL_EXPORT void checkAlcError();
extern FLAT_OAL_EXPORT void checkAlcError(const Device & device);
extern FLAT_OAL_EXPORT void checkError();

extern FLAT_OAL_EXPORT const char * attrName(ALCint key) noexcept;




struct Error : public Flat::Debug::Error {};




struct FLAT_OAL_EXPORT Device
{
	static Device open(const char * name);

	Device() noexcept {}
	Device(Device && other) noexcept : handle(other.handle) { other.handle = nullptr; }
	Device & operator=(Device && other) noexcept { std::swap(handle, other.handle); return *this; }
	~Device() noexcept { if (handle) ::alcCloseDevice(handle); }

	Device(const Device &) = delete;
	void operator=(const Device & other) = delete;

	::ALCdevice * handle = nullptr;

private:
	Device(::ALCdevice * const handle) noexcept : handle(handle) {}
};




struct FLAT_OAL_EXPORT Context
{
	static Context create(const Device & device, const ALCint * attrs);

	Context() noexcept {}
	Context(Context && other) noexcept : handle(other.handle) { other.handle = nullptr; }
	Context & operator=(Context && other) noexcept { std::swap(handle, other.handle); return *this; }
	~Context() noexcept { if (handle) ::alcDestroyContext(handle); }

	Context(const Context &) = delete;
	void operator=(const Context &) = delete;

	::ALCcontext * handle = nullptr;

private:
	Context(::ALCcontext * const handle) noexcept : handle(handle) {}
};




struct FLAT_OAL_EXPORT Buffer
{
	static Buffer create();

	Buffer() noexcept : id(AL_NONE) {}
	~Buffer() noexcept;

	Buffer(Buffer && other) noexcept : id(other.id) { other.id = AL_NONE; }
	Buffer & operator=(Buffer && other) noexcept { std::swap(id, other.id); return *this; }
	operator bool() const noexcept { return id != AL_NONE; }

	ALuint id;

	Buffer(const Buffer & other) = delete;
	void operator=(const Buffer & other) = delete;

private:
	Buffer(const ALuint id) noexcept : id(id) {}
};




struct FLAT_OAL_EXPORT Source
{
	static Source create();

	Source() noexcept : id(AL_NONE) {}
	~Source() noexcept;

	Source(Source && other) noexcept : id(other.id) { other.id = AL_NONE; }
	Source & operator=(Source && other) noexcept { std::swap(id, other.id); return *this; }
	operator bool() const noexcept { return id != AL_NONE; }

	ALuint id;

	Source(const Source & other) = delete;
	void operator=(const Source & other) = delete;

private:
	Source(const ALuint id) noexcept : id(id) {}
};




}}
